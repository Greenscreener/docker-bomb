FROM docker

COPY docker-compose.yml /app/docker-compose.yml
COPY Dockerfile /app/Dockerfile
