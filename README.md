# Docker Bomb
An infinitely recursive docker-compose container. The container builds itself
and then runs itself in itself, which leads to an infinite loop. This is perhaps
the slowest and most boring recursive bomb you can imagine, because you'll be
waiting a looong time for the images to pull each time.

Run `docker compose up`.
